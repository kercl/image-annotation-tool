import { ActionLog, Shapes } from "./actions.js";
import { Canvas } from "./canvas.js";

import { PanTool } from "./tools/pan.js";
import { PointTool } from "./tools/point.js";
import { LineTool } from "./tools/line.js";

const { invoke } = window.__TAURI__.tauri;

const TMP_LOG = "tmp_log";
var action_logs = {
    "tmp_log": new ActionLog()
}

var main_canvas = new Canvas("main-canvas", (ctx, transform) => {
    action_logs[TMP_LOG].renderables.forEach((element) => {
        switch (element.type) {
            case Shapes.Point: {
                const [x, y] = transform.apply_vector(element.x, element.y);
                ctx.beginPath();
                ctx.arc(x, y, 3, 0, 2 * Math.PI, false);
                ctx.fillStyle = "#b62b6e";
                ctx.fill();
                break;
            }
            case Shapes.Line: {
                const [x1, y1] = transform.apply_vector(element.x1, element.y1);
                const [x2, y2] = transform.apply_vector(element.x2, element.y2);

                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y2);
                ctx.lineWidth = 2;
                ctx.strokeStyle = "#b62b6e";
                ctx.stroke();
                break;
            }
        }
    })
});

function select_tool(btn, cls) {
    document.getElementsByName("tool-buttons").forEach((element) => {
        element.style.backgroundColor = null;
    });

    btn.style.backgroundColor = "var(--highlight-color)";
    main_canvas.interact.tool = new cls(main_canvas, action_logs[TMP_LOG]);
}

window.addEventListener("DOMContentLoaded", () => {
    main_canvas.redraw_now();
    select_tool(document.getElementById("tool-pan"), PanTool);

    document.getElementById("btn-undo").addEventListener("click", (_event) => {
        action_logs[TMP_LOG].undo();
        main_canvas.redraw();
    });

    document.getElementById("tool-pan").addEventListener("click", (event) => {
        select_tool(event.target, PanTool);
    });

    document.getElementById("tool-draw-point").addEventListener("click", (event) => {
        select_tool(event.target, PointTool);
    });

    document.getElementById("tool-draw-line").addEventListener("click", (event) => {
        select_tool(event.target, LineTool);
    });

    window.addEventListener("keypress", (event) => {
        switch (event.key) {
            case "m": select_tool(document.getElementById("tool-pan"), PanTool); break;
            case ".": select_tool(document.getElementById("tool-draw-point"), PointTool); break;
            case "l": select_tool(document.getElementById("tool-draw-line"), LineTool); break;
        }
    });
});
