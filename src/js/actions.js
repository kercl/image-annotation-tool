export const Action = {
    DrawPoint: 0,
    DrawLine: 1,
}

export const Shapes = {
    Point: 0,
    Line: 1,
}

export class ActionLog {
    constructor() {
        this.log = [];
        this.renderables = []
    }

    apply_action(action) {
        switch (action.type) {
            case Action.DrawPoint:
                this.renderables.push({
                    type: Shapes.Point,
                    x: action.x,
                    y: action.y,
                });
                break;
            case Action.DrawLine:
                this.renderables.push({
                    type: Shapes.Line,
                    x1: action.x1,
                    y1: action.y1,
                    x2: action.x2,
                    y2: action.y2,
                });
                break;
        }
    }

    regenerate() {
        this.renderables = [];

        this.log.forEach((action) => {
            this.apply_action(action);
        });
    }

    perform(action) {
        this.log.push(action);
        this.apply_action(action);
    }

    undo() {
        this.log.pop();
        this.regenerate();
    }
}
