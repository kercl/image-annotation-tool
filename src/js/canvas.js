import { Interact } from "./interact.js";
import { AffineTransform } from "./math.js";

export class Canvas {
    constructor(id, render_callback) {
        this.id = id;
        this.dom_element = undefined;

        this.transform = new AffineTransform(1, 0, 0, 1, 0, 0);
        this.overlay = {
            cursor_pos: {
                x: "-",
                y: "-",
            },
            tool_line: null,
        }

        self.redraw_queued = true;
        this._draw_trigger = setInterval(() => {
            if (this.redraw_queued) {
                this.redraw_now();
                this.redraw_queued = false;
            }
        }, 20);

        this.render_callback = render_callback;
    }

    element() {
        if (!this.dom_element) {
            this.dom_element = document.getElementById(this.id);

            this.dom_element.addEventListener("mouseover", (event) => {
                this.redraw();
            });

            window.addEventListener("resize", (event) => {
                this.redraw();
            });

            this.interact = new Interact(this);

            this._image_src = document.createElement("img");
            this._image_src.style.display = "none";
            this._image_src.src = "/assets/test.jpg";
            this.dom_element.appendChild(this._image_src);
        }

        this.dom_element.width = this.dom_element.offsetWidth;
        this.dom_element.height = this.dom_element.offsetHeight;

        return this.dom_element;
    }

    context() {
        return this.element()?.getContext("2d");
    }

    canvas_width() {
        return this.element()?.width;
    }

    canvas_height() {
        return this.element()?.height;
    }

    get_transform() {
        return this.transform;
    }

    set_transform(affine_transform) {
        this.transform = affine_transform;
    }

    draw_background(ctx, bw, bh) {
        var w = this.canvas_width();
        var h = this.canvas_height();

        let color_id = 0;
        for (var y = 0; y < h; y += bh) {
            for (var x = bw * (color_id % 2); x < w; x += 2 * bw) {
                ctx.rect(x, y, bw, bh);
            }

            color_id += 1;
        }

        ctx.fillStyle = "#f2f2f2";
        ctx.fill();
    }

    draw_image(ctx) {
        ctx.drawImage(this._image_src, 0, 0);
    }

    draw_overlay(ctx, width, height) {
        ctx.fillStyle = "black";
        ctx.textAlign = "right";

        let [img_space_x, img_space_y] = this.transform.inverse.apply_vector(
            this.overlay.cursor_pos.x, this.overlay.cursor_pos.y);

        img_space_x = Math.round(img_space_x * 100) / 100;
        img_space_y = Math.round(img_space_y * 100) / 100;
        ctx.fillText(`${img_space_x}, ${img_space_y}`, width - 50, height - 50);

        if (this.overlay.tool_line) {
            const offset = Math.round(Date.now() / 100) % 8;

            ctx.beginPath();
            ctx.moveTo(this.overlay.tool_line.x1, this.overlay.tool_line.y1);
            ctx.lineTo(this.overlay.tool_line.x2, this.overlay.tool_line.y2);
            ctx.lineDashOffset = -offset;
            ctx.setLineDash([4, 4]);
            ctx.strokeStyle = "black";
            ctx.stroke();
            ctx.moveTo(this.overlay.tool_line.x1, this.overlay.tool_line.y1);
            ctx.lineTo(this.overlay.tool_line.x2, this.overlay.tool_line.y2);
            ctx.lineWidth = 1;
            ctx.lineDashOffset = -(4 + offset);
            ctx.setLineDash([4, 4]);
            ctx.strokeStyle = "white";
            ctx.stroke();

            this.redraw();

            setTimeout(() => this.redraw(), 100);
        }
    }

    redraw() {
        this.redraw_queued = true;
    }

    redraw_now() {
        var ctx = this.context();
        const width = this.canvas_width();
        const height = this.canvas_height();

        ctx.resetTransform();
        ctx.clearRect(0, 0, width, height);
        this.draw_background(ctx, 30, 30);

        ctx.resetTransform();
        ctx.setTransform(...this.transform.as_array);
        this.draw_image(ctx);

        ctx.resetTransform();
        this.render_callback(ctx, this.transform);

        this.draw_overlay(ctx, width, height);
    }

    update_overlay(obj) {
        this.overlay = { ...this.overlay, ...obj };
        this.redraw();
    }
}
