
function edit_label_name(btn) {
    const label = btn.parentNode.querySelector("label.label-sel-name");
    const edit = btn.parentNode.querySelector("input.label-sel-name");

    label.style.display = "none";
    edit.style.display = "block";

    edit.value = label.textContent;

    confirm_callback = () => {
        label.textContent = edit.value;
        label.style.display = "block";
        edit.style.display = "none";
    };

    edit.onblur = confirm_callback;
    edit.onkeypress = (event) => {
        if (event.key === "Enter") {
            confirm_callback();
        }
    };

    edit.focus();
    edit.select();
}
