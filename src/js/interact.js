import { PanTool } from "./tools/pan.js";

export class Interact {
    constructor(canvas) {
        this.canvas = canvas;

        this.tool = new PanTool(canvas);
        this.pan_tool = new PanTool(canvas);

        canvas.dom_element.addEventListener("mousedown", (event) => { this.mousedown(event) });
        canvas.dom_element.addEventListener("mouseup", (event) => { this.mouseup(event) });
        canvas.dom_element.addEventListener("mouseleave", (event) => { this.mouseleave(event) });
        canvas.dom_element.addEventListener("mousemove", (event) => { this.mousemove(event) });
        canvas.dom_element.addEventListener("wheel", (event) => { this.wheel(event) });
        canvas.dom_element.addEventListener("click", (event) => { this.click(event) });
    }

    mouse_event_data(event) {
        let x = event.clientX - this.canvas.dom_element.offsetLeft;
        let y = event.clientY - this.canvas.dom_element.offsetTop;

        return {
            button: event.button,
            pos: {
                x: x,
                y: y,
            }
        }
    }

    mousedown(event) {
        if(event.button == 1) {
            this.pan_tool.mousedown(this.mouse_event_data(event));
        } else if (this.tool.mousedown) {
            this.tool.mousedown(this.mouse_event_data(event));
        }
    }

    mouseup(event) {
        if(event.button == 1) {
            this.pan_tool.mouseup(this.mouse_event_data(event));
        } else if (this.tool.mouseup) {
            this.tool.mouseup(this.mouse_event_data(event));
        }
    }

    mouseleave(event) {
        this.pan_tool.mouseleave(this.mouse_event_data(event));
        
        if (this.tool.mouseleave) {
            this.tool.mouseleave(this.mouse_event_data(event));
        }
    }

    mousemove(event) {
        const mouse_data = this.mouse_event_data(event);

        this.pan_tool.mousemove(this.mouse_event_data(event));

        if (this.tool.mousemove) {
            this.tool.mousemove(mouse_data);
        }

        this.canvas.update_overlay({
            cursor_pos: {
                x: mouse_data.pos.x,
                y: mouse_data.pos.y,
            }
        });
    }

    wheel(event) {
        let pos = this.mouse_event_data(event);

        (new PanTool(this.canvas)).wheel({
            ...pos, ...{
                dx: event.deltaX,
                dy: event.deltaY,
                dz: event.deltaZ,
            }
        });
    }

    click(event) {
        if (this.tool.click) {
            this.tool.click(this.mouse_event_data(event));
        }
    }
}
