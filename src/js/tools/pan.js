import { AffineTransform } from "../math.js";

export class PanTool {
    constructor(canvas, _log) {
        this.canvas = canvas;
        this.start_event = null;
    }

    mousedown(event) {
        this.start_event = event;
        this.start_transform = this.canvas.get_transform();
    }

    mouseup(event) {
        this.start_event = null;
    }

    mouseleave(event) {
        this.start_event = null;
    }

    mousemove(event) {
        if (this.start_event) {
            let transform = this.start_transform.copy();
            transform.e = event.pos.x - this.start_event.pos.x + this.start_transform.e;
            transform.f = event.pos.y - this.start_event.pos.y + this.start_transform.f;

            this.canvas.set_transform(transform);
            this.canvas.redraw();
        }
    }

    wheel(event) {
        let s = 1.10;
        if (event.dy > 0) {
            s = 1 / s;
        }

        const [e, f] = this.canvas.get_transform().inverse
            .apply_vector(event.pos.x, event.pos.y);

        let result = this.canvas.get_transform()
            .multiply(new AffineTransform(s, 0, 0, s, e * (1 - s), f * (1 - s)));

        this.canvas.set_transform(result);
        this.canvas.redraw();
    }
}
