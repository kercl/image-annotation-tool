import { Action } from "../actions.js";

export class PointTool {
    constructor(canvas, log) {
        this.log = log;
        this.canvas = canvas;
    }

    click(event) {
        if (event.button != 0) {
            return;
        }

        const [x, y] = this.canvas.transform.inverse
            .apply_vector(event.pos.x, event.pos.y);

        this.log.perform({
            type: Action.DrawPoint,
            x: x,
            y: y,
        });
    }
}
