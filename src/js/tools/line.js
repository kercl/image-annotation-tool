import { Action } from "../actions.js";

export class LineTool {
    constructor(canvas, log) {
        this.log = log;
        this.canvas = canvas;
        this.start_event = null;
    }

    mousedown(event) {
        this.start_event = event;
    }

    mouseup(event) {
        this.canvas.update_overlay({
            tool_line: null
        });

        const [x1, y1] = this.canvas.transform.inverse
            .apply_vector(
                this.start_event.pos.x, this.start_event.pos.y);

        const [x2, y2] = this.canvas.transform.inverse
            .apply_vector(
                event.pos.x, event.pos.y);

        this.log.perform({
            type: Action.DrawLine,
            x1: x1,
            y1: y1,
            x2: x2,
            y2: y2,
        });

        this.start_event = null;
    }

    mouseleave(event) {
        this.start_event = null;
        this.canvas.update_overlay({
            tool_line: null
        });
    }

    mousemove(event) {
        if (this.start_event) {
            this.canvas.update_overlay({
                tool_line: {
                    x1: this.start_event.pos.x,
                    y1: this.start_event.pos.y,
                    x2: event.pos.x,
                    y2: event.pos.y,
                }
            });
        }
    }

}
