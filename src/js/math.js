export class AffineTransform {
    /*

    / a c e \
    | b d f |
    \ 0 0 1 /

    */
    constructor(a, b, c, d, e, f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    multiply(other) {
        let a = this.a * other.a + this.c * other.b;
        let b = this.b * other.a + this.d * other.b;
        let c = this.a * other.c + this.c * other.d;
        let d = this.b * other.c + this.d * other.d;
        let e = this.a * other.e + this.c * other.f + this.e;
        let f = this.b * other.e + this.d * other.f + this.f;

        return new AffineTransform(a, b, c, d, e, f);
    }

    apply_vector(x, y) {
        return [
            this.a * x + this.c * y + this.e,
            this.b * x + this.d * y + this.f
        ]
    }

    transform_length(l) {
        const dx = this.a * l, dy = this.b * l;
        return Math.sqrt(dx*dx + dy*dy);
    }

    get as_array() {
        return [this.a, this.b, this.c, this.d, this.e, this.f];
    }

    get det() {
        return this.a * this.d - this.b * this.c;
    }

    get inverse() {
        let det = this.det;
        return new AffineTransform(
            this.d / det,
            -this.b / det,
            -this.c / det,
            this.a / det,
            (this.c * this.f - this.d * this.e) / det,
            (this.b * this.e - this.a * this.f) / det);
    }

    copy() {
        return new AffineTransform(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
